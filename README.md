# LWP : Likelihood Weighing Protocol

---

## Authors

  * Reed Essick
  * Phil Landry
  * Katerina Chatziioannou
  * Isaac Legred
  * Sunny Ng

---

## Scope

This library provides access to the basic workflows employed in [1-5] to compute marginal likelihoods for GW data given a set of candidate equations of state (EoS).
This is done through Monte Carlo sampling from a prior defined over component masses, tidal deformabilities in combination with a likelihood model constructed from a Gaussian kernel density estimate.

LVK analyses may make use of publicly available (nonparametric) EoS posterior samples generated released as part of previous short-author work [6], apply additional astrophysical weights based on new observations with this library, and then use the resulting weighed sample set to place constraints on the pressure-density and/or mass-radius relation.

[1] https://journals.aps.org/prd/abstract/10.1103/PhysRevD.99.084049 \
[2] https://journals.aps.org/prd/abstract/10.1103/PhysRevD.101.063007 \
[3] https://journals.aps.org/prd/abstract/10.1103/PhysRevD.101.123007 \
[4] https://journals.aps.org/prd/abstract/10.1103/PhysRevD.104.063003 \
[5] https://journals.aps.org/prd/abstract/10.1103/PhysRevD.105.043016 \
[6] https://zenodo.org/record/6502467 

---

### Installation

We recommend local pip installation into a virtual environemnt, an environement containing all needed dependencies is provided at `./environments/lwp39.yaml`.
To install :
(for SSH cloning)
```sh
git clone git@git.ligo.org:reed.essick/lwp.git
cd lwp
pip install .
```

---

## Library Structure

*Executables*
See the `lwp/executables` subdirectory for a list of executables,
these can be called from the command line using scripts provided in
`/bin`

* `lwp_pipe`
  The primary functionality of the package is wrapped in the function
  `lwp_pipe` found in `lwp/executables/lwp_pipe.py`. In brief, the function
  weighs given EoSs by an astrophysical likelihood.  

  The function performs
  the following manipulations
  
  [1] As input, a list of EoSs represented by the Mass-Lambda curves
  must be input, as well as posterior samples of an astrophysical PE
  analysis.  Other options may be required, see the function documentation. \
  [2] For each EoS, sample M-Lambda samples according to a fixed chirp-mass,
  mass-ratio prescription, and convert the samples to component masses, while
  reweighing and downsampling to produce a distribution that is flat in
  component masses.  Compute the values of component lambdas based on the EoS. \
  [3] Weigh each sample by a kde of the astrophsyical likelihood.  This is
  computed by building a kde for the posterior sample, with each sample
  divided by the marginal prior for m1-m2-lambda1-lambda2 which must
  be provided as input.  Additionally a bandwidth for the kde must be
  provided as input. \
  [4] Marginalize over the samples to produce a marginal likelihood for each
  EoS. \
  [5] The likelihoods of each M-Lambda sample as well as the marginal
  likelihoods are returned by default.  Flags can be set in the function
  arguments to also save these files to disk. 
 

Much of this code was taken from

  * https://github.com/reedessick/universality

---

## Notes and Examples

Minimal example:

See the notebook `./Examples/lwp-tutorial-190425.ipynb`
for an example of how to run the inference pipeline, including
collecting and formatting data for use in the `lwp_pipe` function.

There are also tests for various functions, which, while not well
documented, can serve as examples for the use of various functions.

A more specific example for review is located in
`Examples/review-lwp-170817-tutorial.ipynb`
This designs a run which specifically mimics the LVK 170817 EoS ananalysis.
It will only run on CIT.

A work in progress is a more extensive notebook which
goes through all functionality in detail.
`Examples/details.ipynb`
You may be able to find something you're looking for there,
such as using bilby results files, or building an EoS sample set.  


# lwp-pipe

A script is included which allows calling the `lwp_pipe` function from the command line.
This additionally allows formatting condor runs, which can speed up things dramatically
because `lwp_pipe` is embarassingly parallel with respect to EoSs used in the inference.

There is an example .ini file in the Examples subdirectrory, the syntax for calling `lwp-pipe` is simple

* `lwp-pipe example.ini`

The call will create an output directory, in this output directory is a subdirectory
called `result`, which contains the likelihoods and marginal likelihoods.  If condor is
used, the input EoS set will be split up into sets.  When all jobs are finished, the results are collated so there is one final likelihood and one final marginalized likelihood file.  See `lwp/submission` for details on submission, rules for config parsing are mainly defined in `lwp/submissing/config.py`, but see also `bin/lwp-pipe` for jobs are distributed,and certain nontrivial

---

## Review Statement
