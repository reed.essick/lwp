import numpy as np
import pandas as pd
import seaborn as sns
import os
import h5py
import subprocess
import bilby


import lwp
import lwp.utils.io as io

from lwp import executables

import unittest
import numpy.testing as npt
import shutil



download_astro_samples = False
download_eos_samples = False

class TestBilbyLoad(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.file_directory = os.path.dirname(__file__)
        self.astro_data, self.bandwidth, self.mc_range, self.q_range = executables.get_files.get_astro_samples(
            f"{self.file_directory}/bilby_pe_result190425.json", 
            "PE190425_low_spin.csv", 
            download_url=None,
            max_num_pe_samples=None,
            load_samples_kwargs = {"load_function": io.load_bilby}).values()
    def test_construction(self):
        npt.assert_array_equal(
            np.array(self.astro_data["m1"]),
            np.array(bilby.core.result.read_in_result(f"{self.file_directory}/bilby_pe_result190425.json").posterior["mass_1_source"]))

    def test_ranges(self):
        npt.assert_array_almost_equal(np.array(self.mc_range), np.array([[1.407193084675286, 1.4704587334111854]]))
        npt.assert_array_almost_equal(np.array(self.q_range), np.array([[0.34933791689121796, 0.9983544890680901]]))
    
if __name__ == "__main__":
    unittest.main()
