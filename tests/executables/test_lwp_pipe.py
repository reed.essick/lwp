import unittest

import numpy as np
from numpy.random import default_rng

import pandas as pd
import os

import lwp.executables as executables

import unittest
import numpy.testing as npt

class TestLwpPipe(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.file_directory = os.path.dirname(__file__)
        self.macro = pd.DataFrame({"M":np.linspace(.8, 2.3, 10),
                                   "Lambda": .2* np.linspace(10, 1, 10)**4})
        self.seed = 123456
        self.rng = default_rng(seed=self.seed) # random number generator, see
        # https://numpy.org/doc/stable/reference/random/index.html#random-quick-start
        self.num_data = 10
        self.m1 = self.rng.uniform(1.4, 1.6, self.num_data)
        self.m2 = self.rng.uniform(1.2, 1.4, self.num_data)
        self.lambda1 = self.rng.uniform(100, 300, self.num_data)
        self.lambda2 = self.rng.uniform(400, 800, self.num_data)
        self.luminosity_distance = self.rng.uniform(30, 50, self.num_data)
        
        self.astro_data = pd.DataFrame({"m1": self.m1, "m2": self.m2,
                                        "Lambda1": self.lambda1,
                                        "Lambda2": self.lambda2,
                                        "luminosity_distance_Mpc": self.luminosity_distance })
        self.results = executables.lwp_pipe(
            eos_indices = np.array([0]), retrieve_macro_data = lambda index : self.macro,
            gw_posterior_samples = self.astro_data,
            likelihood_bandwidth = 1.0,
            save_likelihoods=f"{self.file_directory}/test_likelihoods_post.csv",
            save_marginalized_likelihoods=f"{self.file_directory}/test_marginal_likelihoods_eos.csv",
            num_marginalization_points=10, mc_marginalization_range=(1.0, 1.3),
            q_marginalization_range=(.6, 1.0), seed=self.seed )

        
        self.target_prior =  'mc_range = (1.0, 1.3), q_range = (0.6, 1.0), dist = default'
        self.target_eos_results =pd.DataFrame(
            np.array([[0 , -3.657212, -8.545248, 10]]),
            columns=["eos",  "logmargweight",  "logvarmargweight", "num_elements"] )
        
    @classmethod
    def tearDownClass(self):
        os.remove(f"{self.file_directory}/test_likelihoods_post.csv")
        os.remove(f"{self.file_directory}/test_marginal_likelihoods_eos.csv")
    def test_simple(self):
        print(self.results)
        assert np.all(self.results["eos_likelihoods"].columns ==
                         self.target_eos_results.columns)
        npt.assert_array_almost_equal(np.array(self.results["eos_likelihoods"]),
                                      np.array(self.target_eos_results))
        self.assertEqual(self.target_prior, self.results["prior"])
        
    
    def test_read(self):
        written_result = pd.read_csv(f"{self.file_directory}/test_likelihoods_post.csv")
        like_key= "likelihood_evaluations"
        assert np.all(
            written_result.columns == self.results[like_key].columns)
        npt.assert_array_almost_equal(
            np.array(written_result), 
            np.array(self.results[like_key]))
        npt.assert_array_almost_equal(
            np.array(self.results[like_key].iloc[0, :]),
            np.array([ 0, 1.445977, 1.33678, 286.635115,
                       431.429335, -6.003079]))
if __name__ == '__main__':
    unittest.main()
