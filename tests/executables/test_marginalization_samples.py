import unittest

import numpy as np
from numpy.random import default_rng

import pandas as pd
import os
from scipy.interpolate import interp1d
import scipy.optimize as optimize

import lwp.executables as executables
import lwp.utils.branched_interpolator as branched_interpolator

import unittest
import numpy.testing as npt
def M_of_rhoc(rhoc):
    return (rhoc-2)**3 - (rhoc-2) + 1
def lambda_of_rhoc(rhoc):
    return  1000*(rhoc+1)**-6
class TestLwpPipe(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.file_directory = os.path.dirname(__file__)
        self.macro_single_branch = pd.DataFrame({"M":np.linspace(.8, 2.3, 100),
                                   "Lambda": .2* np.linspace(10, 1, 100)**4})
        rhoc = np.linspace(1.0, 3.0, 100)
        self.M_of_rhoc = M_of_rhoc
        test_M =  self.M_of_rhoc(rhoc)
        self.lambda_of_rhoc = lambda_of_rhoc
        test_Lambda = self.lambda_of_rhoc(rhoc)
        self.macro_multi_branch = pd.DataFrame({"M" : test_M, "Lambda":test_Lambda})
        self.seed = 12345
        self.rng = default_rng(seed=self.seed)
        
    @classmethod
    def tearDownClass(self):
        os.remove(f"{self.file_directory}/single_branch_samps.csv")
        os.remove(f"{self.file_directory}/multi_branch_samps.csv")
    def test_branches(self):
        self.assertEqual(len(branched_interpolator.get_branches(self.macro_single_branch)), 1)
        self.assertEqual(len(branched_interpolator.get_branches(self.macro_multi_branch)), 2)
    def test_singlebranch(self):
        """
        Testing if we generate consistent samples for a single stable branch
        """
        samples = executables.marginalization_samples(
            self.macro_single_branch,
            output=f"{self.file_directory}/single_branch_samps.csv",
            eos_index=0, nsamps=10, save_samples=True, seed=self.seed, mcrng=(.9,1.0))
        lambda_of_m = interp1d(self.macro_single_branch["M"],
                               self.macro_single_branch["Lambda"],
                               bounds_error=False, fill_value=0.0)
        m1 = samples["m1"]
        lambda1_found = samples["Lambda1"]
        lambda1_computed = lambda_of_m(m1)
        npt.assert_array_almost_equal(lambda1_found, lambda1_computed, decimal=3)
        
    def test_multibranch(self):
        """
        Test if we generate consistent samples from multiple stable branches
        """
        samples = executables.marginalization_samples(
            self.macro_multi_branch,
            output=f"{self.file_directory}/multi_branch_samps.csv",
            eos_index=0, nsamps=10, save_samples=True, seed=self.seed, mcrng=(.9,1.0))
        m1 = samples["m1"]
        Lambda1_found = samples["Lambda1"]
        for pair in zip(m1, Lambda1_found):
            # If the interpolation is reasonable,
            # then the m-Lambda point will lie on M(rhoc), Lambda(rhoc)
            # for some rhoc, it doesn't matter what it is,
            # but if there is some value the cost will be low
            result= optimize.least_squares(
                lambda rhoc : (
                    np.array([M_of_rhoc(rhoc[0]),
                              lambda_of_rhoc(rhoc[0])]) - np.array(pair))/(np.array(pair)+1),
                    x0=np.array([.1]))
            self.assertLess(result.cost,  1e-1)
if __name__ == '__main__':
    unittest.main()
