import numpy as np
import pandas as pd
import seaborn as sns
import os
import h5py
import subprocess


import lwp
import lwp.utils.io as io
import lwp.submission.config as lwp_config

from lwp import executables

import unittest
import numpy.testing as npt
import shutil



download_astro_samples = False
download_eos_samples = False

class TestDownload(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        print("working")
        self.file_directory = os.path.dirname(__file__)
        download_link = "https://zenodo.org/record/6513631/files/IGWN-GWTC2p1-v2-GW19\
0425_081805_PEDataRelease_mixed_nocosmo.h5?download=1," if download_astro_samples else None
        astro_data_and_metadata = executables.get_files.get_astro_samples(
            "IGWN-GWTC2p1-v2-GW190425_081805_PEDataRelease_mixed_nocosmo.h5", 
            f"./PE190425_low_spin.csv", 
            download_url=download_link,
            max_num_pe_samples=500,
            load_samples_kwargs = {"load_function": io.load_public_h5_samples, "low_spin":True})
        self.astro_prefix= "PE190425_low_spin"
        self.astro_data = astro_data_and_metadata["data"]
        self.bandwidth = astro_data_and_metadata["bandwidth"]
        self.mc_range = astro_data_and_metadata["mc_range"]
        self.q_range = astro_data_and_metadata["q_range"]
        
        eos_samples_url = "https://zenodo.org/record/6502467/files/LCEHL_EOS_posterior_samples_PSR.h5?download=1"
        self.eos_samples_file =  "LCEHL_EOS_posterior_samples_PSR.h5"
        if download_eos_samples:
            executables.get_files.download(eos_samples_url, self.eos_samples_file)
        
        
        samples_tag = "np"
        eos_samples  = h5py.File(self.eos_samples_file)
        
        self.eos_to_be_used=np.arange(10)
        self.macro_data = {eos_num: np.array(eos_samples['ns'][eos_id])
                      for eos_num, eos_id in enumerate(eos_samples['eos'])}
        self.outdir = "DefaultOutdir"
        if os.path.exists(self.outdir):
            shutil.rmtree(self.outdir)
        
        
    def test_dump_config(self):
        pd.DataFrame(np.arange(10), columns=["eos"]).to_csv("eos_indices.csv", index=False)
        result = executables.lwp_pipe(
            eos_indices = np.array(self.eos_to_be_used),
            retrieve_macro_data = lambda index: self.macro_data[index], 
            gw_posterior_samples = self.astro_data,
            likelihood_bandwidth=self.bandwidth, 
            save_likelihoods=f"./{self.astro_prefix}_post.csv",
            save_marginalized_likelihoods=f"./{self.astro_prefix}_eos.csv",
            seed=12345,
            dump_config="example_test_standard.ini",
            dump_config_kwargs={"config_kwargs": {"eos-indices": "eos_indices.csv"},
                        "lwp_pipe_data_kwargs":{"gw_posterior_samples":
                                               "PE190425_low_spin.csv",
                                               "eos_samples_h5_path":self.eos_samples_file, 
                                               "eos_samples_h5_macro_subgroup":"ns",
                                               "eos_samples_h5_index_subgroup":"eos",
                                               "outdir":f"{self.outdir}"}})
        self.assertEqual(result["Samples"]["gw-posterior-samples"],
                         "PE190425_low_spin.csv")

    def test_run_results(self):
        print(self.astro_data)
        result = executables.lwp_pipe(
            eos_indices = np.array(self.eos_to_be_used),
            retrieve_macro_data = lambda index: self.macro_data[index], 
            gw_posterior_samples = self.astro_data,
            likelihood_bandwidth=self.bandwidth,
            seed=12345,
            save_likelihoods=None,
            save_marginalized_likelihoods=None)
        current_env = os.environ
        subprocess.call(["lwp-pipe", "example_test_standard.ini"], env=current_env)
        written_marginal_likelihoods = pd.read_csv(f"{self.outdir}/result/{self.astro_prefix}_eos.csv")
        npt.assert_array_almost_equal(np.array(written_marginal_likelihoods), np.array(result["eos_likelihoods"]))
    def test_alternative_get_configs(self):
        """
        Uh oh, I forgot to put in paths to the gw_posterior_samples,
        and eos_indices! What should happen is these files get written to a 
        special location specified by the random seed
        """
        result = executables.lwp_pipe(
            eos_indices = np.array(self.eos_to_be_used),
            retrieve_macro_data = lambda index: self.macro_data[index], 
            gw_posterior_samples = self.astro_data,
            likelihood_bandwidth=self.bandwidth, 
            save_likelihoods=f"./{self.astro_prefix}_post.csv",
            save_marginalized_likelihoods=f"./{self.astro_prefix}_eos.csv",
            seed=12345,
            dump_config="example_test_forgotten.ini",
            dump_config_kwargs={"config_kwargs": {},
                        "lwp_pipe_data_kwargs":{
                            "eos_samples_h5_path":self.eos_samples_file, 
                            "eos_samples_h5_macro_subgroup":"ns",
                            "eos_samples_h5_index_subgroup":"eos",
                            "outdir":f"{self.outdir}"}})
        config = lwp_config.configparser.ConfigParser()
        config.read("example_test_forgotten.ini")
        pipe_data = lwp_config.lwp_pipe_data()
        pipe_data.from_config(config)
        self.assertEqual(config["Samples"]["gw-posterior-samples"],
                         "gw_posterior_samples_45823.csv")
        self.assertEqual(config["Samples"]["eos-indices"],
                         "eos_indices_45823.csv")
        eos_indices_written = pd.read_csv("eos_indices_45823.csv")
        self.assertEqual(list(eos_indices_written.keys()), ["eos"])
        npt.assert_array_equal(np.array(eos_indices_written["eos"]),
                               np.array(self.eos_to_be_used))
        
if __name__ == "__main__":
    unittest.main()
