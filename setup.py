#!/usr/bin/env python
__usage__ = "setup.py command [--options]"
__description__ = "standard install script"
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

from setuptools import (setup, find_packages)
import glob

#-------------------------------------------------

# set up arguments
scripts = glob.glob('bin/*')
packages = find_packages()
package_data = {}

# install
setup(
    name = 'lwp',
    version = '0.0.0',
    url = 'https://git.ligo.org/reed.essick/lwp',
    author = __author__,
    author_email = 'reed.essick@gmail.com',
    description = __description__,
    license = 'MIT',
    scripts = scripts,
    packages = packages,
    package_data = package_data,
    requires = [], ### FIXME: specify requirements for numpy, h5py, lalsuite, else?
)
