"""
Extract the PE needed to launch a run using cbcflow to find tidal PE

This assumes cbcflow is installed and that the user's ~/.cbcflow.cfg 
has something like 
`library =  /home/<user>/<path>/<to>/cbc-workflow-o4a `
and that the library is up to date

See the cbcflow docs
https://cbc.docs.ligo.org/projects/cbcflow/configuration.html#

One will also need EoS samples downloaded in order to run this script, 
it currently uses the samples from zenodo, see the examples for a guide to
downloading them.
EoS nonparametric samples needed: 
LCEHL_EOS_posterior_samples_PSR.h5
"""
import pandas as pd
import numpy as np

import cbcflow

from lwp.executables import (lwp_pipe, get_files)
import lwp.utils.io as io
                             
def get_results_file(event_name, pattern_to_match={"Notes":  "PhenomPv2_NRTidalv2"}):
    """
    Find a bilby result file on disk based on cbcflow metadata PE results.  
    The function takes an event name by superevent, and a dictionary of field:pattern 
    pairs.  Return a list of PE Result Files from runs which have the given pattern in the 
    field given
    """
    metadata = cbcflow.get_superevent(event_name)
    pe_runs = metadata["ParameterEstimation"]["Results"]
    meet_criteria = [
        run for run in pe_runs if
        all([any([pattern_to_match[field] in elt for elt in run[field]]) for
             field in pattern_to_match.keys()])]
    return list(map(lambda run : run["ResultFile"]["Path"], meet_criteria))

if __name__ == "__main__":
    event_name = "S230529ay"
    result_files_with_phenom_pv2_nrtidalv2 = get_results_file(event_name)
    result_file = result_files_with_phenom_pv2_nrtidalv2[0].split(":")[1]
    # Compute the likelihood details, including the optimal bandwidth
    likelihood_metadata = get_files.get_astro_samples(specific_file_name=result_file, scan_likelihood=False,
                                                      dataframe_outpath=f"{event_name}_pv2_nrtidalv2.csv",
                                                      load_samples_kwargs = {"load_function" : io.load_bilby})
    # Create a fake eos_indices file for demonstration purposes
    pd.DataFrame(np.arange(10), columns=["eos"]).to_csv("eos_indices.csv", index=False)
    # Write the lwp call to a config file
    result = lwp_pipe(
        eos_indices = np.arange(10),
        retrieve_macro_data = None, 
        gw_posterior_samples = pd.read_csv(f"{event_name}_pv2_nrtidalv2.csv"),
        likelihood_bandwidth= likelihood_metadata["bandwidth"], 
        save_likelihoods=f"./cbcflow_{event_name}_post.csv",
        save_marginalized_likelihoods=f"./cbcflow_{event_name}_eos.csv",
        seed=12345,
        dump_config=f"example_cbcflow_{event_name}.ini",
        dump_config_kwargs={"config_kwargs": {"eos-indices": "eos_indices.csv"},
                            "lwp_pipe_data_kwargs":{"gw_posterior_samples":
                                                    f"{event_name}_pv2_nrtidalv2.csv",
                                                    "eos_samples_h5_path":"./LCEHL_EOS_posterior_samples_PSR.h5", 
                                                    "eos_samples_h5_macro_subgroup":"ns",
                                                    "eos_samples_h5_index_subgroup":"eos",
                                                    "outdir":f"cbcflow_{event_name}_outdir"}})

    


