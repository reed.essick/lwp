import configparser
import os 
config = configparser.ConfigParser()

cwd = os.getcwd()
def local(path, cwd):
  return os.path.join(cwd, path)
config['Samples'] = {'eos-directory': '/home/isaac.legred/parametric-eos-priors/eos_draws/gw170817_eos_draw_spectral',
                     'eos-per-dir': 100 ,
                     'eos-indices-tuple': '(0, 100)',
                     'gw-posterior-samples':
                     local('LVC_GW170817_PhenomPNRT-lo_small.csv', cwd),
                     'bandwidth': local('LVC_GW170817_PhenomPNRT-lo_small.in', cwd)}
config['Marginalization'] = {'prior' : 'default',
                             'chirp-mass-range' : '(.8, 1.6)',
                             'mass-ratio-range' : '(.2, 1.0)'
                             }
config['Submission'] = {'label' : 'example',
                        'format-for-condor': 'True',
                        'condor-num-jobs': '10',
                        'accounting' : 'ligo.sim.o4.cbc.extremematter.bilby',
                        'submit-dag' : 'True',
                        'merge-executable' : '/home/isaac.legred/lwp/bin/combine-samples',
                        'condor-kwargs' : {'request_disk' : '10 MB',
                                           'request_memory': '2048 MB',
                                           'universe' : 'vanilla'
                                           }
                        }
config["Output"] = {'save-marginalized-likelihoods' : 'gw170817_eos.csv',
                    'save-likelihoods': 'gw170817_post.csv',
                    'output-dir': local("example_run", cwd)}

with open('example.ini', 'w') as configfile:
  config.write(configfile)


# with open('example.ini', 'r') as configfile:
#   config.read(configfile)

