#!/usr/bin/env python

'SCAN-LIKELIHOOD -- read likelihood data and estimate optimal bandwidth for kde representation'
__usage__ = 'scan-likelihood likedata.csv [-c m1 m2 Lambda1 Lambda2 Prior -v]'
__author__ = 'Philippe Landry (pgjlandry@gmail.com)'
__date__ = '12-2020'

from argparse import ArgumentParser
import numpy as np
import os

from lwp.kde import optimize_bandwidth
from lwp.utils import io
from lwp.executables.scan-likelihood import scan_likelihood

parser = ArgumentParser(description=__doc__)
parser.add_argument('likedata',nargs="*")
parser.add_argument('-d', '--delim', help='delimiter for input data file, DEFAULT=","', default=',')
parser.add_argument('-c', '--cols', help='name of likelihood data column(s) to use, DEFAULT=ALL_COLS', default=False, nargs='+')
parser.add_argument('-p', '--pctile', help='percentile for truncation of samples in kde representation (3-sigma, by default), DEFAULT=0.15', default=0.15, type=float)
parser.add_argument('-b', '--bwrng', help='range for kde optimal bandwidth search, DEFAULT=[0.01,0.51]', default=False, nargs=2)
parser.add_argument('-n', '--numproc', help='number of processes to spawn for optimal bandwidth search, DEFAULT=50', default=50, type=int)
parser.add_argument('-i', '--iters', help='maximum number of iterations for optimal bandwidth search, DEFAULT=10', default=10, type=int)
parser.add_argument('-t', '--tol', help='tolerance for optimal bandwidth, DEFAULT=1e-3', default=1e-3, type=float)
parser.add_argument('-m', '--maxsamps', help='maximum number of likelihood samples to use in bandwidth search, DEFAULT=ALL', default=False)
parser.add_argument('-o', '--outpath', help='paths for likelihood metadata and bandwidth log output (appends .in and .bw to likelihood file name, respectively, by default), DEFAULT=AUTO', default=False, nargs=2)
parser.add_argument('--prior_name', help='name of prior for samples, DEFAULT=None', default=None)
parser.add_argument('--prior_columns', help='names of columns used for prior, DEFAULT=None', default=None,nargs="*")
parser.add_argument('-v', '--verbose', action='store_true', default=False)
args = parser.parse_args()

###

DELIM = ","

###

if __name__ == "__main__":
        scan_likelihood(args.likedata, args.delim, args.cols, args.pctile,
                args.bwrng, args.numproc, args.iters,
                args.tol, args.maxsamps, args.outpath, args.prior_name, args.prior_columns, args.verbose)
