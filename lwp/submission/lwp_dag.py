import os
import pycondor
import copy
import numpy as np
import pandas as pd

from dataclasses import dataclass

import lwp.submission.config as configure
_configurations_dir = "Configurations"
@dataclass
class CondorArgs:
    name : str
    executable : str 
    condor_num_jobs : int = None
    accounting : str = None
    arguments : list[tuple] = None
    outdir : str = None
    condor_kwargs : dict = None

    @staticmethod
    def from_config(config, executable, job_id=1, queue=1):
        executable = executable
        outdir = config["Output"]["output-dir"]

        read_name = configure.read_optional(config["Submission"], "label")
        if read_name is None:
            # The default name is the executable name, which must exist
            name = f"{executable}-{job_id}"
        else:
            name = f"{read_name}-{job_id}"
        # We assume that each job is unique (i.e. queue=1)
        condor_num_jobs = queue
        accounting = configure.read_optional(config["Submission"], "accounting") 
        # The only arguement to the job is the ini file
        # This could change in the future
        arguments = os.path.join(outdir, _configurations_dir,
                                 f"{name}-split.ini")
        condor_kwargs = configure.read_optional(config["Submission"], "condor-kwargs",
                                                parse=configure.parse_dict)
        return CondorArgs(name, executable,  condor_num_jobs, accounting,
                          arguments, outdir, condor_kwargs)
class Job:
    """
    Manage condor job creation
    """
        
    def __init__(self, condor_args):
        self.name = condor_args.name
        self.executable = condor_args.executable
        self.queue = condor_args.condor_num_jobs # Each job is only run once
        self.kwargs = condor_args.condor_kwargs
        self.accounting = condor_args.accounting
        self.arguments = condor_args.arguments
        self.output= os.path.join(condor_args.outdir, "runinfo", "output")
        self.log =os.path.join(condor_args.outdir, "runinfo", "log")
        self.error =os.path.join(condor_args.outdir, "runinfo", "error")
        self.submit =os.path.join(condor_args.outdir, "submit")
        
        self.job = pycondor.Job(self.name, self.executable,
                                queue=self.queue,
                                arguments=self.arguments,
                                output=self.output,
                                error=self.error,
                                log=self.log,
                                submit=self.submit,
                                extra_lines=[f"accounting_group = {self.accounting}"],
                                getenv=True,
                                **self.kwargs)
    def reform_job(self):
         self.job = pycondor.Job(self.name, self.executable,
                                 queue=self.queue,
                                 arguments=self.arguments,
                                 output=self.output,
                                 error=self.error,
                                 log=self.log,
                                 submit=self.submit,
                                 extra_lines=[f"accounting_group = {self.accounting}"],
                                 getenv=True,
                                 **self.kwargs)
    def append_arg(self, arg):
        self.arguments.append(arg)
        self.job.add_arg(arg)

class MergeJob(Job):
    def __init__(self, condor_args, result_dir, merge_name="result"):

        super().__init__(condor_args)
        self.name = f"merge-{merge_name}-{self.name}"
        self.queue = 1 # Only a single merge
        self.arguments = []
        self.result_dir = result_dir
        self.reform_job()
        # This code should be changed to avoid ridiculous things
        # like this 
        self.arguments = [""]
    def add_parent(self, parent_job, parent_output_file = None):
        parent_job.job.add_child(self.job)
        self.arguments[0] += f" {parent_output_file}"

        

class Dag:
    def __init__(self, name, submit, **dag_kwargs):
        self.name = name
        self.submit = submit
        self.dag_kwargs = dag_kwargs
        self.dag = pycondor.Dagman(self.name, submit=self.submit, **self.dag_kwargs)

    def add_job(self, job):
        self.dag.add_job(job)

    def build(self, **kwargs):
        return self.dag.build(**kwargs)
    def build_submit(self, **kwargs):
        return self.dag.build_submit(**kwargs)
    
        
    
class PipeSubmission:
    """
    The syntax here is special as the executable only takes the config file
    the default behavior is to split the eos_indices among the provided jobs
    """
    def __init__(self, config, executable, **dag_kwargs):
        read_name = configure.read_optional(config["Submission"], "label")

        if read_name is None:
            # The default name is the executable name, which must exist
            name = f"{executable}"
        else:
            name = f"{read_name}"
   

        self.config = config
        num_jobs = configure.read_optional(config["Submission"],
                                           "condor-num-jobs",
                                           parse=configure.parse_int)
        if num_jobs is None:
            num_jobs = 1

        pipe_data = configure.lwp_pipe_data()
        pipe_data.from_config(config)
        all_eos_indices = pipe_data.eos_indices
        job_eos_indices = np.array_split(all_eos_indices, num_jobs)
        outdir = config["Output"]["output-dir"]
        os.mkdir(os.path.join(outdir, _configurations_dir))
        self.dag = Dag(name, submit=os.path.join(outdir, "submit"),
                       **dag_kwargs)
        self.jobs = []
        merge_executable = configure.read_optional(config["Submission"],
                                                   "merge-executable")
        
        if merge_executable is not None:
            merge_condor_args = CondorArgs.from_config(config,
                                                       executable=merge_executable)
            merge_likelihoods = MergeJob(merge_condor_args,
                                         result_dir=os.path.join(outdir, "result"),
                                         merge_name="likelihood")
            merge_marg_likelihoods = MergeJob(merge_condor_args, 
                                              result_dir=os.path.join(outdir, "result"),
                                              merge_name="marg-likelihood")
            
        result_dir = os.path.join(outdir, "result")
        for job_id in range(num_jobs):
            like_arg = "save-likelihoods"
            marg_like_arg = "save-marginalized-likelihoods"
            job_config = self.clone_config()
            # For the individual jobs, we use the input file as the
            # arguements

            job_config["Submission"]["format-for-condor"] = "False"
            eos_df = pd.DataFrame(job_eos_indices[job_id], columns=["eos"])
            indices_path =os.path.join(outdir, _configurations_dir,
                                       f"eos_indices_{job_id}.csv")
            eos_df.to_csv(indices_path, index=False)

            job_config["Samples"]["eos-indices"] = indices_path
            job_config["Output"][like_arg] = (
                f'run-{job_id}-{job_config["Output"][like_arg]}')
            job_config["Output"][marg_like_arg] = (
                f'run-{job_id}-{job_config["Output"][marg_like_arg]}'
            )
            job_config["Output"]["derived"] = "True"
            job_condor_args = CondorArgs.from_config(job_config,
                                                     executable=executable,
                                                     job_id=job_id,
                                                     queue=1)
            # The arguments field stores where this job will
            # get it's .ini from, i.e. this guarantees the job's args
            # will be read from the correct place
            with open(job_condor_args.arguments, 'w') as job_configfile:
                job_config.write(job_configfile)
            this_job = Job(job_condor_args)
            self.dag.add_job(this_job.job)
            if merge_executable is not None:
                merge_likelihoods.add_parent(this_job,
                                     parent_output_file = os.path.join(
                                         result_dir,
                                         job_config["Output"][like_arg]))
                merge_marg_likelihoods.add_parent(this_job,
                                    parent_output_file = os.path.join(
                                        result_dir,
                                        job_config["Output"][marg_like_arg]))
        if merge_executable is not None:
            # Last arg is name of output
            merge_likelihoods.arguments[0] += (
                f' --output-name {result_dir}/{config["Output"][like_arg]}') 
            merge_marg_likelihoods.arguments[0] += (
                f' --output-name {result_dir}/{config["Output"][marg_like_arg]}')
            # Set the arguments correctly
            merge_likelihoods.job.add_arg(merge_likelihoods.arguments[0])
            merge_marg_likelihoods.job.add_arg(merge_marg_likelihoods.arguments[0])
            self.dag.add_job(merge_likelihoods.job)
            self.dag.add_job(merge_marg_likelihoods.job)

    def build(self, **kwargs):
        if configure.read_optional(self.config["Submission"],
                                   "submit-dag",
                                   parse=configure.parse_bool):
            self.dag.build_submit(fancyname=False, **kwargs)
        else:
            self.dag.build(fancyname=False, **kwargs)
    def clone_config(self):
        return copy.deepcopy(self.config)
    
