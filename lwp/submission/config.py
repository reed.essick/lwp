import configparser
import os
import pandas as pd
import numpy as np
from dataclasses import dataclass 
import warnings

import lwp.utils.io as io
import lwp.utils as utils


def default_failure(input, input_type):
    typename = repr(input_type).split("'")[1]
    raise ValueError (f" \'{input}\' not parsable to {typename}")

# Some of these things are alternatively implemented directly
# in the config library, we can improve them over time. 

def parse_tuple(input, type_fun=float):
    if input is None:
        return None
    return tuple(type_fun(k.strip()) for k in input[1:-1].split(','))
def unparse_tuple(input, unparse_elt=lambda x : x):
    input = tuple((unparse_elt(elt) for elt in input))
    return str(input)

def parse_list(input, type_fun=str):
    if input ==  "None":
        return None
    return list(type_fun(k.strip()) for k in input[1:-1].split(','))
def unparse_list(input, unparse_elt=lambda x: str(x)):
    if input == "None":
        return None
    input = [unparse_elt(elt) for elt in input]
    return f"[{','.join(input)}]"
    


def parse_dict(input, parse_key=lambda k: k, parse_value=lambda v: v):
    if input == "None":
        return None
    else:
        pairs = [pair_str.split(":") for pair_str in input[1:-1].split(",")]
        pair_dict = {parse_key(pair[0].strip().strip("'")):
                     parse_value(pair[1].strip().strip("'")) for pair in pairs}
        return pair_dict
    

# Special case for bools, which have bad non-failure behavior even when args
# are misformed
def parse_bool(input, default=lambda input : default_failure (input, bool)):
    if input == "None":
        return None
    if input in ["True", "true"]:
        return True
    elif input in ["False", "false"]:
        return False
    else:
        default(input)
        
def parse_int(input):
    if input == "None":
        return None
    return int(input)

def parse_float(input):
    if input == "None":
        return None
    return float(input)

def parse_csv_filename(input):
    if input == "None":
        return None
    return pd.read_csv(input)

loading_map = {"csv" : io.load_csv,
               "default": None,
               "bilby" : io.load_bilby,
               "public": io.load_public_h5_samples}
def parse_loading_strategy(input):
    if input is None:
        return io.load_samples
    else:
       
        if input in loading_map.keys():
            return loading_map[input]
        else:
            return io.load_samples
            warnings.warn("Unknown loading strategy, using"
                          "the default strategy, which is based"
                          "on file extensions.")
def get_csv_path(variable, paths):
    return paths[variable]
def write_loading_strategy(strat):
    if strat is None:
        return "default"
    return {v: k for (k, v) in loading_map.items()}[strat]
def read_optional(group, location, parse=lambda x : x):
    if location in group.keys():
        return parse(group[location])
    else :
        return None
def write_optional(data, group, location, unparse=lambda x : str(x)):
    if data is not None:
        group[location] = unparse(data)

            
@dataclass
class lwp_pipe_data:
    eos_directory : str = None
    eos_per_dir : int = None
    eos_indices : np.ndarray = None
    eos_samples_h5_path: str = None
    eos_samples_h5_macro_subgroup: str = None
    eos_samples_h5_index_subgroup: str = None 
    gw_posterior_samples : str = None
    gw_posterior_loading_strategy : str = "default"
    max_num_pe_samples : int = None
    astro_columns : list[str] = None
    prior_columns : list[str] = None
    likelihood_bandwidth : float = None
    likelihood_prior_key : str = None
    mc_marginalization_range : tuple[float] = None
    q_marginalization_range : tuple[float] = None
    num_marginalization_points : int = None
    marginalization_mass_prior : str = None
    num_proc : int = None
    seed : int = None
    save_likelihoods : str = None
    save_marginalized_likelihoods: str = None
    outdir : str = None
    def from_config(self, config):
        """
        Construct an lwp-pipe arguments parameter set from a config file,
        this function will fail if the needed parameters to do a run don't 
        exist, and will set all optional arguments to None if they are 
        not present in the config
        """
        # If Samples group isn't present code should fail 
        self.eos_directory = read_optional(config["Samples"], "eos-directory")
        self.eos_per_dir = read_optional(config["Samples"], "eos-per-dir",
                                         parse=parse_int)
        if self.eos_directory is None or self.eos_per_dir is None:
            # We don't have enough information for a full eos prior,
            # look for an h5 eos set
            self.eos_samples_h5_path = read_optional(
                config["Samples"], "eos-samples-h5-path")
            self.eos_samples_h5_macro_subgroup = read_optional(
                config["Samples"], "eos-samples-h5-macro-subgroup")
            self.eos_samples_h5_index_subgroup = read_optional(
                config["Samples"], "eos-samples-h5-index-subgroup")
        if self.eos_directory is None and self.eos_samples_h5_path is None:
            raise ValueError("No complete EoS sample set specified!")
        if read_optional(config["Samples"], "eos-indices")  is None:
            self.eos_indices= np.arange(
                *parse_tuple(config["Samples"]["eos-indices-tuple"]), dtype=int)
        else:
            self.eos_indices = np.array(
                pd.read_csv(config["Samples"]["eos-indices"])["eos"], dtype=int)
        self.gw_posterior_samples = config["Samples"]["gw-posterior-samples"]
        self.gw_posterior_loading_strategy = read_optional(
            config["Samples"], "gw-posterior-loading-strategy")
        self.max_num_pe_samples = read_optional(config["Samples"], "max-num-pe-samples",
                                                  parse=parse_int)
        self.astro_columns = read_optional(config["Samples"], "astro-columns",
                                           parse=parse_list)
        self.prior_columns = read_optional(config["Samples"], "prior-columns",
                                           parse=parse_list)

        self.likelihood_bandwidth = read_optional(config["Samples"], "likelihood-bandwidth",
                                           parse=parse_float)
        if self.likelihood_bandwidth is None:
            bandwidth_file = read_optional(config["Samples"], "bandwidth-file",
                                       parse=parse_csv_filename)
            self.likelihood_bandwidth = bandwidth_file["bw"][0]
        self.likelihood_prior_key = read_optional(config["Samples"], "likelihood-prior-key")

        if "Marginalization" in config.keys():
            
            self.mc_marginalization_range = (
                read_optional(config["Marginalization"], "chirp-mass-range",
                              parse=parse_tuple)
            )
            self.q_marginalization_range = (
                read_optional(config["Marginalization"], "mass-ratio-range",
                              parse=parse_tuple)
            )
            self.num_marginalization_points = (
                read_optional(config["Marginalization"],
                              "num-marginalization-points", parse=parse_int)
            
            )
            if self.num_marginalization_points is None:
                self.num_marginalization_points = 50
            self.marginalization_mass_prior = read_optional(config["Marginalization"],
                                                   "mass-distribution")
        if "Submission" in config.keys():
            self.num_proc = read_optional(config["Submission"], "num-proc",
                                          parse=parse_int)

            if self.num_proc is None:
                self.num_proc = utils.DEFAULT_NUM_PROC
            self.seed = read_optional(config["Submission"], "seed",
                                          parse=parse_int)

        if "Output" in config.keys():
            self.outdir = read_optional(config["Output"], "output-dir")
            self.save_likelihoods = read_optional(config["Output"],
                                                  "save-likelihoods")
            self.save_marginalized_likelihoods = (
                read_optional(config["Output"],
                            "save-marginalized-likelihoods")
            )
    
    def to_config(self, write_path="example.ini", write_config_kwargs=None):
        """
        Do the inverse thing of from config, but also write the config
        if specified

        write_config_kwargs get written directly to the config, if they
        are processable
        """
        config = configparser.ConfigParser()
        # If no EoS set is specified then this config will not work
        if self.eos_directory is None and self.eos_samples_h5_path is None:
            raise ValueError("No complete EoS sample set specified!")
        config["Samples"] = {}
        write_optional(self.eos_directory, config["Samples"], "eos-directory")
        write_optional(self.eos_per_dir, config["Samples"], "eos-per-dir")
        # We don't have enough information for a full eos prior,
        # look for an h5 eos set
        write_optional(self.eos_samples_h5_path,
                       config["Samples"], "eos-samples-h5-path")
        write_optional(self.eos_samples_h5_macro_subgroup,
                      config["Samples"], "eos-samples-h5-macro-subgroup")
        write_optional(self.eos_samples_h5_index_subgroup,
                       config["Samples"], "eos-samples-h5-index-subgroup")

        # This must be present in write_config_kwargs
        write_optional(self.eos_indices, config["Samples"], "eos-indices",
                       unparse=lambda _ : write_config_kwargs["eos-indices"])
        write_optional(self.gw_posterior_samples,config["Samples"], "gw-posterior-samples")
        write_optional(self.gw_posterior_loading_strategy,
            config["Samples"], "gw-posterior-loading-strategy")
        write_optional(self.max_num_pe_samples, config["Samples"], "max-num-pe-samples") 
        write_optional(self.astro_columns, config["Samples"], "astro-columns",
                                           unparse=unparse_list)
        write_optional(self.prior_columns, config["Samples"], "prior-columns",
                                           unparse=unparse_list)
        write_optional(self.likelihood_bandwidth, config["Samples"], "likelihood-bandwidth") 
        write_optional(self.likelihood_prior_key, config["Samples"],
                       "likelihood-prior-key")
        
        config["Marginalization"] = {}
        
        write_optional(self.mc_marginalization_range, config["Marginalization"],
                       "chirp-mass-range",
                       unparse=unparse_tuple)
        write_optional(self.q_marginalization_range, config["Marginalization"],
                       "mass-ratio-range",
                       unparse=unparse_tuple)
        write_optional(self.num_marginalization_points, config["Marginalization"],
                       "num-marginalization-points")

        write_optional(self.marginalization_mass_prior, config["Marginalization"],
                       "mass-distribution")
        config["Submission"] = {}
        write_optional(self.num_proc, config["Submission"], "num-proc")
        write_optional(self.seed, config["Submission"], "seed")
        config["Output"] = {}
        
        write_optional(self.outdir, config["Output"], "output-dir")
        write_optional(self.save_likelihoods, config["Output"],
                       "save-likelihoods")
        write_optional(self.save_marginalized_likelihoods,
                       config["Output"],
                       "save-marginalized-likelihoods")
        if write_path is not None:
            with open(write_path, 'w+') as configfile:
                config.write(configfile)
        return config
