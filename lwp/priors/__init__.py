"""a module that houses basic tools for reweighting posterior samples to a different prior
"""
__author__ = "Philippe Landry (philippe.landry@ligo.org)"

#-------------------------------------------------

from .priors import *
