#!/usr/bin/env python

import numpy as np
import astropy.units as u
from astropy.coordinates import Distance
from astropy.cosmology import Planck15 as cosmo
import bilby.gw.prior as bilby_prior
import bilby.core.prior.analytical as an_prior

### Class for prior dict object
class lwp_pe_mass_prior:
    
    def __init__(self, function):
        self.function_ = function
    def prob(self, sample):
        return self.function_(*(sample[key] for key in sample.keys()))

### BASIC FUNCTIONS

def src_to_det(m,z): # convert to detector frame mass from source frame
    
        return m*(1.+z)
    
def dL_to_z(dL): # convert distance to redshift, assuming a cosmology
    
        return Distance(dL,unit=u.Mpc).compute_z(cosmology=cosmo)
        
### BINARY MASS PRIORS
def _verify_m1_geq_m2(m1, m2):
        return np.array(m1 > m2, dtype=float)

def flat_m1m2(m1,m2,dL): # uniform prior in source frame masses, subject to m1 >= m2 convention


        val = _verify_m1_geq_m2(m1, m2)
    
        return val

def flat_m1m2_quad_dL(m1,m2,dL): # flat in detector frame masses, subject to m1 >= m2 convention and quadratic prior in dL
    

        val = dL**2 * _verify_m1_geq_m2(m1, m2)
    
        return val

def flat_m1m2det(m1,m2,dL): # flat in detector frame masses, subject to m1 >= m2 convention and flat prior in dL
    
        val = (1.+dL_to_z(dL))**2 * _verify_m1_geq_m2(m1, m2)
    
        return val
        
def flat_m1m2det_quad_dL(m1,m2,dL): # flat in detector frame masses, subject to m1 >= m2 convention and quadratic prior in dL
    

        val = dL**2*(1.+dL_to_z(dL))**2 * _verify_m1_geq_m2(m1, m_2)
    
        return val

def flat_mceta(m1,m2,dL): # flat in chirp mass and symmetric mass ratio
    
        
        val = (m1-m2)*(m1*m2)**0.6/(m1+m2)**3.2 * _verify_m1_geq_m2(m1, m2)
    
        return val

def flat_mcetadet(m1,m2,dL): # flat in chirp mass and symmetric mass ratio, assuming flat prior in dL
    

        val = (1.+dL_to_z(dL))**2*(m1-m2)*(m1*m2)**0.6/(m1+m2)**3.2 * _verify_m1_geq_m2(m1, m2)
    
        return val
    
def flat_mcq(m1,m2,dL): # flat in chirp mass and mass ratio
    
        val = (m1*m2)**0.6/(m1**2*(m1+m2)**0.2) * _verify_m1_geq_m2(m1, m2)
    
        return val

def flat_mcqdet(m1,m2,dL): # flat in chirp mass and mass ratio, assuming flat prior in dL
    
        
        val = (1.+dL_to_z(dL))*(m1*m2)**0.6/(m1**2*(m1+m2)**0.2) * _verify_m1_geq_m2(m1, m2)
    
        return val

def gauss_mc_flat_eta(m1,m2,dL,mu,sigma): # gaussian in chirp mass, flat in symmetric mass ratio
    
        #Need to calculate chirp mass given m1, m2, and utilize error propagation through change of variables
        
        ### Jacobian transformation from Mc and eta --> m1, m2:
        jacob = (((m1*m2)**(0.6))*(m1-m2))/((m1+m2)**(3.2))
        Mc = ((m1*m2)**(0.6))/((m1+m2)**(0.2))
        val = ((1./(np.sqrt(2*np.pi)*(sigma))*(np.exp((-(Mc-mu)**2)/(2*(sigma**2)))))*jacob)*_verify_m1_geq_m2(m1, m2)
        
        return val
    
def gauss_mc_flat_q(m1,m2,dL,mu,sigma): # gaussian in chirp mass, uniform in mass ratio q
    
        #Need to calculate chirp mass given m1, m2, and utilize error propagation through change of variables
        
        ### Jacobian transformation from Mc and q --> m1, m2:
        Mc = ((m1*m2)**(0.6))/((m1+m2)**(0.2))
        jacob = ((Mc)/(m1**2))
        val = ((1./(np.sqrt(2*np.pi)*(sigma))*(np.exp((-(Mc-mu)**2)/(2*(sigma**2)))))*jacob)*_verify_m1_geq_m2(m1, m2)
        
        return val

### PRIOR LOOKUP FUNCTIONS
    
binary_mass_priors = {'flat_m1m2': flat_m1m2, 'flat_m1m2det': flat_m1m2det, 'flat_mceta': flat_mceta, 'flat_mcetadet': flat_mcetadet, 'flat_m1m2det_quad_dL': flat_m1m2det_quad_dL, 'flat_m1m2_quad_dL': flat_m1m2_quad_dL, 'flat_mcq': flat_mcq , 'flat_mcqdet': flat_mcqdet}

def get_binary_mass_prior(pe_mass_prior):
    
        if isinstance(pe_mass_prior, bilby_prior.PriorDict):
            return pe_mass_prior
        elif isinstance(pe_mass_prior, str):
            return lwp_pe_mass_prior(binary_mass_priors[pe_mass_prior])
        elif isinstance(pe_mass_prior, dict):
            return bilby_prior.PriorDict({key: get_single_prior(value) for key, value in parameter_prior.items()})
        else:
            raise ValueError("Can't parse parameter prior.")

        # try: prior_func = binary_mass_priors[key]
        # except KeyError:
        #         print('Invalid prior specification, accepted keys are as follows:\n{0}\n'.format(binary_mass_priors.keys()))
        #         raise KeyError

        # return prior_func    
            
def get_bilby_prior(func_name):
    
    try:
        prior_name = functions[func_name]
        
    except:
        raise KeyError(f"Prior not available not within listed analytical priors on Bilby. Available priors: {functions}")
        return
    
    analytical_prior = getattr(an_priors, f"{prior_name}")
    return analytical_prior

def get_single_prior(parameter_prior):
    
    if isinstance(parameter_prior, prior.Prior):
        return parameter_prior
    elif isinstance(parameter_prior, str):
        return get_bilby_prior(parameter_prior)
    else:
        raise ValueError("Can't parse parameter prior.")
    
        
