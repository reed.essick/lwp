__author__ = "isaac.legred@caltech.edu"


import os
import numpy as np
import pandas as pd

from argparse import ArgumentParser

def combine_samples(input_files, output_name, 
                    columns=None, verbose=False, Verbose=False):
    """
    Given many files containing samples, produce a common file with the same columns as each individual file
    containing all the samples.  Only transfer the column names specified by an array-like `columns`
    """
    # Copy the first input file to get the output dataframe (
    # this will fail if there are no input files)
    print("input_files are", input_files)
    output_data = pd.read_csv(input_files[0]).copy()

    # Concatenate all the dataframes into one dataframe
    for i, input_file  in enumerate(input_files[1:]):
        output_data = pd.concat([output_data, 
                                 pd.read_csv(input_file)],
                                ignore_index=True, axis=0, join="inner")
        
        shared_columns = output_data.columns
        if columns is None:
            columns = shared_columns
        else:
            for column in shared_columns:
                if column not in columns:
                    # remove columns that were'nt requested
                    output_data.drop(column)
    
        
    

    
    if verbose:
        print(f"writing collated results into: {output_name}")
    output_data.to_csv(output_name, index=False)
