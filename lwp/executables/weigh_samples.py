__author__ = "Reed Essick (reed.essick@gmail.com)"
#-------------------------------------------------

import numpy as np
import pandas as pd

from argparse import ArgumentParser

### non-standard libraries
from lwp.utils import (utils, io)
from lwp import kde
from lwp import priors

#-------------------------------------------------


def weigh_samples(source, target, output, columns, prior_columns, verbose=False, logcolumn=[],
                  weight_column=[], weight_column_is_log=[],
                  invert_weight_column=[],
                  prior_name = "flat_m1m2det", ###
                  column_range=[],
                  reflect=False, prune=False,
                  output_weight_column=utils.DEFAULT_WEIGHT_COLUMN,
                  do_not_log_output_weights=False, bandwidth=0.03,
                  num_proc=utils.DEFAULT_NUM_PROC, save_samples=True):
    
    Ncol = len(columns)
    ## Backwards compatibility ##
    source_is_str = False
    target_is_str = False
    if isinstance(source, str):
        source = pd.read_csv(source)
        source_is_str = True

    if isinstance(target, str):
        target = pd.read_csv(target)
        target_is_str = True

    ### parse ranges
    rangesdict = dict()
    for column, _min, _max in column_range:
        assert column in columns, \
               f'specifying --column-range for unknown column: {column}'
        rangesdict[column] = (float(_min), float(_max))

    #-------------------------------------------------

    ### read in source samples
    if verbose:
        print('reading source samples from: ', source)
    srcdata, columns = io.load(source, columns, logcolumns=logcolumn)
    srcdata_prior, prior_columns = io.load(source, prior_columns, logcolumns=logcolumn)
    if not len(srcdata):
        raise RuntimeError('no samples present in %s!'%source)
    srcdata, srcmeans, srcstds = utils.whiten(srcdata, verbose=verbose) ### whiten data

    if prior_name is not None:
        prior_weight_function = priors.get_binary_mass_prior(prior_name)
        srcdf = pd.DataFrame(data = srcdata_prior, columns = prior_columns)
        weights = np.array(prior_weight_function.prob(srcdf))
        shape = len(weights)
        
    else:
        srcdf = pd.DataFrame(data = srcdata, columns = columns)
        shape = srcdf.shape[0]
        weights = np.ones((1,shape))
    
    ### figure out ranges based on data
    ranges = []
    for i, col in enumerate(columns):
        if col in rangesdict.keys():
            m, M = rangesdict[col]
            ranges.append(((m-srcmeans[i])/srcstds[i], (M-srcmeans[i])/srcstds[i]))
        else:
            ranges.append(None)

    if prune:
        srcdata, weights = utils.prune(srcdata, ranges, weights=weights)

    if reflect:
        srcdata, weights = utils.reflect(srcdata, ranges, weights=weights)

    #------------------------

    ### read in target samples
    if verbose:
        print("reading in target samples from: "+target)
    tgtdata, tgtcolumns = io.load(target, logcolumns=logcolumn) ### load in all the columns!
    io.check_columns(tgtcolumns, columns, logcolumns=logcolumn) ### make sure we have the columns we need to

    if len(tgtdata): ### we need to weigh things

        tgtsamples = np.empty((len(tgtdata), Ncol), dtype='float')
        for i, column in enumerate(columns): ### whiten the target data with the same transformation used for source data
            tgtsamples[:,i] = (tgtdata[:,tgtcolumns.index(column)] - srcmeans[i])/srcstds[i]

        #---------------------------------------------

        if verbose:
            print('computing weighted KDE at %d samples from %s based on %d samples from %s with %d cores'%\
                (len(tgtsamples), target, len(srcdata), source, num_proc))
        logkde = kde.logkde(
            tgtsamples,
            srcdata,
            np.ones(Ncol, dtype=float)*bandwidth**2,
            weights=weights,
            num_proc=num_proc
        )
    else:
        if verbose:
            print('no target samples in %s; nothing to do...'%target)
        logkde = np.empty(0, dtype=float) ### make a place-holder so the rest of the logic holds

    if do_not_log_output_weights:
        if verbose:
            print('exponentiating weights')
        logkde = np.exp(logkde)

    if verbose:
        print('writing results with weight-column=%s into: %s'%(output_weight_column, output))

    ### undo logcolumn formatting if needed
    for column in logcolumn:
        i = tgtcolumns.index(io.column2logcolumn(column))
        tgtdata[:,i] = np.exp(tgtdata[:,i])
        tgtcolumns[i] = column

    ### now actually write stuff to disk
    atad = np.empty((len(tgtdata), len(tgtcolumns)+1), dtype=float)
    atad[:,:-1] = tgtdata
    atad[:,-1] = logkde
    if save_samples:
        io.write(output, atad, tgtcolumns+[output_weight_column])
    return pd.DataFrame(atad, columns=tgtcolumns+[output_weight_column])
