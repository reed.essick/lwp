from argparse import ArgumentParser
import numpy as np
import os


def reweight_masses(data, newprior, delim=",", cols=['m1', 'm2', 'dL', 'z'],
                    oldprior="flat_m1m2", outpath=None, maxsamps=None,
                    verbose=False):
    if outpath:
        out_path = outpath
    else:
        out_path = os.path.dirname(
            data)+'/'+os.path.basename(data).split('.')[-2]+'_reweighted.csv'

    # get specified prior distributions

    old_prior = priors.get_binary_mass_prior(oldprior)
    new_prior = priors.get_binary_mass_prior(newprior)

    # load posterior samples

    post_data = np.genfromtxt(data, names=True,
                              dtype=None, delimiter=delim)
    cols = post_data.dtype.names
    if verbose:
        print(cols)

    m1s = post_data[cols[0]]
    m2s = post_data[cols[1]]
    dLs = post_data[cols[2]]
    zs = post_data[cols[3]]

    if maxsamps:

        idxs = np.random.choice(range(len(m1s)), int(maxsamps), False)
        m1s = [m1s[idx] for idx in idxs]
        m2s = [m2s[idx] for idx in idxs]
        dLs = [dLs[idx] for idx in idxs]
        zs = [zs[idx] for idx in idxs]

    post_samps = list(zip(m1s, m2s, dLs, zs))
    num_samps = len(m1s)
    # assume equally weighted posterior samples
    post_weights = np.ones(num_samps)

    # calculate old prior weight for each posterior sample

    old_prior_weights = [old_prior(*samp[:-1]) for samp in post_samps]

    # calculate new prior weight for each posterior sample

    new_prior_weights = [new_prior(*samp[:-1]) for samp in post_samps]

    # reweight posterior via (new prior) * (likelihood), where likelihood = (posterior) / (old prior)

    new_post_weights = [post_weight*(new_prior_weight/old_prior_weight) for post_weight, old_prior_weight,
                        new_prior_weight in zip(post_weights, old_prior_weights, new_prior_weights)]

    # save reweighted posterior

    new_post_samps = np.column_stack((np.array(m1s), np.array(m2s), np.array(dLs),
                                      np.array(zs),
                                      np.array([1./wt for wt in old_prior_weights]),
                                      np.array(new_post_weights)))
    col_names = 'm1_source,m2_source,dL,z,likelihood,posterior'
    np.savetxt(out_path, new_post_samps, header=col_names,
               comments='', delimiter=',')
