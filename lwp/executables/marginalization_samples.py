import numpy as np
from argparse import ArgumentParser
from scipy.interpolate import interp1d
from numpy.random import uniform, choice
import os
import pandas as pd

import lwp.utils.branched_interpolator as branched_interpolator

def marginalization_samples(macro, output, eos_index, nsamps = 50, mcrng=[.9, 2.2], qrng=[0.1, 1.0], save_samples=True, eos_column_name="eos", seed=None):
    """Sample M1, Lambda1, M2, Lambda2 for a particualr EoS 
    """
    
    # load csv containing a tabulated M-Lambda curve (backwards compatibility)
    macro_is_str = False
    if isinstance(macro, str):
        macro_is_str = True
        macro = np.genfromtxt(macro, names=True, delimiter=',', dtype=None)
   
    # Will this generically not be zero?
    if eos_index is not None:
        eos = np.full(nsamps, eos_index, dtype=int)
    else:
        # Assume EoS is encoded in the name of the macro file
        
        ### Change ###
        inferred_eos_index = int(os.path.splitext(macro)[0].split("/")[-1])
        eos = np.full(int(nsamps), inferred_eos_index, dtype=int)
        ##############
    np.random.seed(seed)
    mcs = uniform(mcrng[0], mcrng[1], 100*nsamps) # Is 100 just a conveniant choice?
    qs = uniform(qrng[0], qrng[1], 100*nsamps)

    # compute the weight of the chirp mass, mass ratio sample under a uniform m1, m2 prior
    wts = mcs * (1.0 + qs) ** 0.4 / qs ** 1.2
    
    idxs = choice(100*nsamps, nsamps, True, wts/np.sum(wts))
    m1s = mcs[idxs] * (1.+qs[idxs])**0.2 / qs[idxs]**0.6
    m2s = m1s * qs[idxs] 

    # If a NS is unstable it is a black hole, and therefore has zero tidal deformability
    # so these interpolators will return 0 if there is no stable branch available,
    # otherwise, if there is a single stable branch that value is used,
    # otherwise a value from any of the stable branches is chosen at random.  
    m1s  = branched_interpolator.choose_lambda_per_m(m1s, macro)["m"]
    L1s = branched_interpolator.choose_lambda_per_m(m1s, macro)["lambda"]
    m2s = branched_interpolator.choose_lambda_per_m(m2s, macro)["m"]
    L2s = branched_interpolator.choose_lambda_per_m(m2s, macro)["lambda"]
    
    samples = pd.DataFrame(np.column_stack((eos,m1s,m2s,L1s,L2s)),
                           columns=[eos_column_name,'m1','m2','Lambda1','Lambda2'])
    
    if save_samples:
        samples.to_csv(output, index=False)

    return samples
