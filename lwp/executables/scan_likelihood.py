from argparse import ArgumentParser
import numpy as np
import pandas as pd
import os
import lwp.priors as priors
from lwp.kde import optimize_bandwidth
from lwp.utils import io, utils

DELIM = ","

def scan_likelihood(likedata, delim=DELIM, cols=None, pctile=0.15,
                    bwrng=[0.01, 0.51], numproc=50, iters=10,
                    tol=1e-3, maxsamps=None, outpath=None, prior_name=None, prior_columns=None, 
                    verbose=False, include_samples_hash=False):
  for likepath in likedata:
        if verbose:
            print('Processing {0}...'.format(likepath))
        if os.path.dirname(likepath) == "":  # If passes a local path this will be needed
            likepath = os.path.join(os.getcwd(), likepath)
        likemeta = []
        likedata = np.genfromtxt(likepath, dtype=None, delimiter=delim,names=True)
        if include_samples_hash:
          print("hash of file name is", hash(likepath), "this isn't quite what we want")
        if cols:
            likevars = cols
        else:
           likevars = likedata.dtype.names

        if outpath:
            bwpath = outpath[1]
        else:
           bwpath = os.path.dirname(likepath)+'/'+os.path.basename(likepath).split('.')[-2]+'.bw'

        if bwrng:
            bwrng = bwrng
        else:
           bwrng = [0.01,0.51]

        if maxsamps:
            maxsamps = maxsamps
        else:
           maxsamps = len(likedata[likevars[0]])

        if verbose:
            print('Searching for optimal bandwidth...')

        ldata, columns = io.load(likepath, cols)
        
        if prior_name is not None:
            
            if prior_columns is not None:
                srcdata_prior, prior_columns = io.load(likepath, prior_columns)
            else:
                srcdata_prior, prior_columns = [ldata, columns]
            
            prior_weight_function = priors.get_binary_mass_prior(prior_name)
            srcdf = pd.DataFrame(data = srcdata_prior, columns = prior_columns)
            wts = prior_weight_function(*map(lambda prior_column : srcdf[prior_column], prior_columns))
        
        else:
            shape = ldata.shape[0]
            wts = np.ones(shape)

        white_ldata, means, stds = utils.whiten(ldata)  # whiten data before passing to bandwidth finder

        optbw = optimize_bandwidth(white_ldata, bwrng, tol, 1, wts)[0]

        for likevar in likevars:

            samps = likedata[likevar]
            lb = np.percentile(samps, pctile)
            ub = np.percentile(samps, 100.-pctile)

            likemeta.append([likevar, lb, ub,optbw])

        if 'm1' in likevars and 'm2' in likevars:

            m1_samps = likedata['m1']
            m2_samps = likedata['m2']
            mc_samps = [(m1*m2)**0.6/(m1+m2)**0.2 for m1, m2 in zip(m1_samps, m2_samps)]
            q_samps = [m2/m1 for m1, m2 in zip(m1_samps, m2_samps)]

            mc_lb = np.percentile(mc_samps, pctile)
            mc_ub = np.percentile(mc_samps, 100.-pctile)

            q_lb = np.percentile(q_samps, pctile)
            q_ub = np.percentile(q_samps, 100.-pctile)

            likemeta.append(['mc', mc_lb, mc_ub,optbw])
            likemeta.append(['q', q_lb, q_ub,optbw])

        likemeta = np.array(likemeta)

        if verbose:
            print('Saving metadata...')

        if outpath: ### edit this to change file created name 
            outpath = "KDE Bandwidth (inverted)"
            
        else:
            outpath = os.path.dirname(
               likepath)+'/'+os.path.basename(likepath).split('.')[-2]+'.in'
        np.savetxt(outpath, likemeta, fmt='%s',delimiter=delim,header='var,lb,ub,bw',comments='')
