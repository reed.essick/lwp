# Importing Standard Libraries ---------------------
import numpy as np
import pandas as pd
import os
import h5py
import warnings

# --------------------------------------------------
import lwp.utils as utils
import lwp.utils.io as io
import lwp.executables as executables
import gzip
def download(url, specific_file_name = None):
    
    #Simple executable to download data with curl given a specific website url
    
    #If the inputted url has a "/" at the end of it, the curl may not work
    #but also the file_name split method will definitely not work unless otherwise specified
    if url[-1] == "/":
        url = url[:-1]
    
    if specific_file_name:
        file_name = specific_file_name
    else:
        file_name = os.path.split(url)[-1]
    
    #Download the data, saved with the title of "file_name"
    cmd = f"curl {url} -o {file_name}"
    
    os.system(cmd)
    
    return

def get_astro_samples(specific_file_name,
                      dataframe_outpath,
                      download_url=None,
                      max_num_pe_samples=utils.DEFAULT_MAX_PE_SAMPLES,
                      load_samples_kwargs={},
                      scan_likelihood=True,
                      scan_likelihood_kwargs={"cols":io.default_lwp_analysis_columns},
                      verbose=True):
    """
    Take a raw astrophysical posterior from GW PE and return the data
    needed for lwp_pipe, including the data in a dataframe and the optimal
    bandwidth for the likelihood kde.  Also return the range of chirp mass and 
    mass ratio samples.  

    Inputs:
    specific_file_name : str
      The path to a file that holds posterior samples, should have a format which
      can be read by the utils.io loading utilities.  
    dataframe_outpath : str 
      Path to save the resulting lwp-style dataframe to as a csv.
    download_url : str
      If not None, download this file which contains the astro samples
      Unzip the file if it is compressed
    max_num_pe_samples : int
      If not None, downsample the PE samples to this number
    load_samples_kwargs: dict
      Passed to io.load_samples, may include a load_function, which specifies
      how to convert the data to an lwp-style dataframe.  If empty, io.load_samples
      will do its best to infer the correct loading strategy.
      Ex. load_samples_kwargs = {"load_function" : io.load_bilby} to guarantee bilby
      loading is used on a bilby results file (though it will be used by default)
    scan_likelihood: bool
      If True, compute the bandwidth for the samples (see executables.scan_likelihood)
    scan_likelihood_kwargs: dict
      kwargs passed to scan_likelihood, (see executables.scan_likelihood)
    verbose: bool
      print helpful information.
    
    Returns:
    astro_data_and_metadata: dict
       dict of: 
         "data": astrophysical samples in dataframe with lwp-format
         "bandwidth": the optimal bandwidth for the kde
         "mc_range": range of chirp mass values in the posterior
         "q_range": range of mass ratio values in the posterior
    Caveats:
      In general it is impossible to extract the prior used in an analysis from
      the posterior samples because even in the LVK releases, the prior is
      not consistently present.  Bilby result files should contain the marginal
      prior.  It is up to the user to guarantee the prior string passed to 
      `lwp_pipe` corresponds correctly to the posterior used in the analysis 
      (i.e. the posterior passed here).  Typically the LVK uses flat in m1 and m2 in 
      detector frame, which is the default for `lwp_pipe` calls
    """
    if download_url is not None:
        download(download_url, specific_file_name)
        if "gz" in specific_file_name:
            with gzip.open(specific_file_name) as comp_file:
                specific_file_out = f"{os.path.splitext(specific_file_name)[0]}"
                with open(specific_file_out, "wb+") as outfile:
                    outfile.write(comp_file.read())
            
            specific_file_name=specific_file_out
    data = io.load_samples(specific_file_name, **load_samples_kwargs)
    if verbose:
        print(f"successfully got {data.shape[0]} pe samples,")
    if max_num_pe_samples is not None:
        downsample_to = min(max_num_pe_samples, data.shape[0])
        data = data.sample(n = downsample_to)
        if verbose:
            print(f"downsampling to {downsample_to} samples")

    
    outpath_prefix, outpath_ext= os.path.splitext(dataframe_outpath)
    if outpath_ext != ".csv":
        dataframe_outpath = f"{outpath_prefix}.csv"
        warnings.warn("dataframe must be written to a csv, modifying outpath to"
                      "represent a .csv path, new dataframe_outpath is "
                      "{dataframe_outpath}")
        
    data.to_csv(dataframe_outpath, index=False)
    if scan_likelihood:
        executables.scan_likelihood([dataframe_outpath], **scan_likelihood_kwargs,
                                    verbose=verbose)
    
    bandwidth = pd.read_csv(f"{outpath_prefix}.in")["bw"][0]
    mc_range = pd.read_csv(f"{outpath_prefix}.in", index_col = "var").loc[["mc"]][["lb", "ub"]]
    q_range = pd.read_csv(f"{outpath_prefix}.in", index_col="var").loc[["q"]][["lb", "ub"]]
    
    return {"data" : data, "bandwidth" : bandwidth, "mc_range" : mc_range, "q_range" :q_range}

