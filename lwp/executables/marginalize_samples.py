import os
import sys
import numpy as np
import pandas as pd

from collections import defaultdict
from argparse import ArgumentParser

# non-standard libraries
from lwp.utils import (utils, io)


def marginalize_samples(samples, columns, weight_column=[],
                        weight_column_is_log=[],
                        max_num_samples=io.DEFAULT_MAX_NUM_SAMPLES,
                        verbose=False,
                        output_path=None,
                        save_samples=True):
    """
    marginalize over all weights associated with combinations of columns, 
    creating a new file with marginalized weights within it
    """
    
    
    if verbose:
        print('reading samples from: ', samples)
    data, columns = io.load(samples, columns, max_num_samples=max_num_samples)

    if weight_column:
        if verbose:
            print('reading in non-trivial weights from: ', samples)
        logweights = io.load_logweights(
            samples,
            weight_column,
            logweightcolumns=weight_column_is_log,
            max_num_samples=max_num_samples,
        )

    else:
        N = len(data)
        logweights = np.zeros(N, dtype='float')

    # now need to marginalize over samples
    if verbose:
        print('marginalizing over weights to determine effective weights for unique combinations of (%s)' % (
            ', '.join(columns)))
    results, columns = utils.marginalize(data, logweights, columns)

    if save_samples:
      # write output CSV file
      if output_path is not None:
          if verbose:
              print('writing marginalized results to: ', output_path)

          outdir = os.path.dirname(output_path)
          if outdir and (not os.path.exists(outdir)):
              os.makedirs(outdir)

      else:
          output_path = sys.stdout  # write to STDOUT

      io.write(output_path, results, columns)
    return pd.DataFrame(results, columns=columns)
