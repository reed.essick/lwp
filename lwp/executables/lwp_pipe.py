import numpy as np
import pandas as pd
import seaborn as sns
import os

import lwp

from lwp import executables
from lwp.utils import (utils, io)
import lwp.submission.config as lwp_config

def _combine_samples(index_dictionary):
    return pd.concat(index_dictionary.values(), ignore_index=True)
_eos_column_name="eos"
def _format_prior(mc_range, q_range, distribution):
    name = "default" if distribution is None else distribution
    return f"mc_range = {mc_range}, q_range = {q_range}, dist = {name}"
# Used in getting a config from an lwp_pipe call, see lwp.submission.config
def get_lwp_pipe_data(eos_indices, 
                      gw_posterior_samples,
                      astro_columns=None,
                      prior_columns=["m1", "m2", "luminosity_distance_Mpc"], ###
                      max_num_pe_samples=utils.DEFAULT_MAX_PE_SAMPLES,
                      marginalization_mass_prior=None,
                      save_likelihoods=None,
                      save_marginalized_likelihoods=None,
                      load_samples_kwargs={},
                      num_proc=utils.DEFAULT_NUM_PROC,
                      num_marginalization_points=50,
                      mc_marginalization_range=(0.8, 1.6), ### Tighten mc_range to (1.18, 1.19) for GW170817
                      q_marginalization_range=(.2, 1.0),
                      likelihood_bandwidth=.03,
                      likelihood_prior_key = 'flat_m1m2det',
                      seed=None, lwp_pipe_data_kwargs={}, verbose=False):
    """
    Get the lwp_config.lwp_pipe_data, which is used to write a config.

    lwp_pipe_data_kwargs will be passed directly to the lwp_pipe_data constructor
    but intermediately  we check to see if gw_posterior_samples has been
    specified, if not, then this means we have no way to associate the samples 
    with a location on disk, so we write the samples to disk and put a pointer
    to the p

    TODO : Macro-data storage seems impossible with default schemes , 
    but it can be handled with file_identification args
    """
    # We generate a random tag to write files to
    rng = np.random.default_rng(seed=seed)
    seed_tag = rng.integers(2**16-1)        
    if "gw_posterior_samples" not in lwp_pipe_data_kwargs.keys():
        if isinstance(gw_posterior_samples, str):
            lwp_pipe_data_kwargs["gw_posterior_samples"] = gw_posterior_samples
        else:
            path = f"gw_posterior_samples_{seed_tag}.csv"
            lwp_pipe_data_kwargs["gw_posterior_samples"] = path
            gw_posterior_samples.to_csv(path, index=False)
    if "load_function" in load_samples_kwargs:
        gw_posterior_loading_strategy = lwp_config.write_loading_strategy(
            load_samples_kwargs["load_function"])
    else:
        gw_posterior_loading_strategy = "default"            
    return lwp_config.lwp_pipe_data(
        **lwp_pipe_data_kwargs,
        eos_indices=eos_indices,
        gw_posterior_loading_strategy=gw_posterior_loading_strategy,
        astro_columns=astro_columns,
        prior_columns=prior_columns,
        max_num_pe_samples=max_num_pe_samples,
        likelihood_bandwidth=likelihood_bandwidth,
        likelihood_prior_key=likelihood_prior_key,
        mc_marginalization_range=mc_marginalization_range,
        q_marginalization_range=q_marginalization_range,
        num_marginalization_points=num_marginalization_points,
        marginalization_mass_prior=marginalization_mass_prior,
        save_likelihoods=save_likelihoods,
        save_marginalized_likelihoods=save_marginalized_likelihoods,
        num_proc=num_proc,
        seed=seed)
    
    
# This is the main function call of the entire codebase
def lwp_pipe(eos_indices, retrieve_macro_data, 
             gw_posterior_samples,
             astro_columns=None,
             prior_columns=["m1", "m2",
                            "luminosity_distance_Mpc"], ###
             max_num_pe_samples=utils.DEFAULT_MAX_PE_SAMPLES,
             marginalization_mass_prior=None,
             save_likelihoods=None,
             save_marginalized_likelihoods=None,
             load_samples_kwargs={},
             num_proc=utils.DEFAULT_NUM_PROC,
             num_marginalization_points=50,
             mc_marginalization_range=(0.8, 1.6), ### Tighten mc_range to (1.18, 1.19) for GW170817
             q_marginalization_range=(.2, 1.0), likelihood_bandwidth=.03,
             likelihood_prior_key = 'flat_m1m2det', seed=None,
             dump_config=None, dump_config_kwargs={}, verbose=False):

    """
    Weigh each equation of state represented in eos_indices by 
    the data represented in gw_posterior samples by monte carlo
    sampling the distribution marginalization_mass_prior.  
    
    Requires : 
      eos_indices: array-like of int 
        A numerical list of eos_indices to use,
        must be unique integer identifier of eos
      retrieve_macro_data: function: int -> table with columns "M", "Lambda"  
        a function which takes an eos_index and 
        returns the m-Lambda curve associated with
        the eos_index.  Ex. eos_indices = [1, 2, 3]
        macro_data = lambda index : macro_files[index]
        where macro_files is a dictionary of 
        {index : m-lambda_data} pairs
      gw_posterior_samples: Dataframe or str 
        A pandas dataframe of
        m1-m2-lambda1-lambda2 posterior samples for a cbc
        event.  Alternatively  a file path name which can
        be interpreted by io.load_samples.  In this case, 
        consider specifying explicitly the loading function
        in load_samples_kwargs, an Optional argument.  
      
    Optional:
      astro_columns : list[str]
        The columns to read from astrophysical posterior 
        samples. The column names should agree with their names in the 
        file being read, for example, if a bilby result.json file is being 
        read, then the column names should be the names bilby assigns the 
        columns. If this arg is None (recommended), the necessary columns will be 
        loaded regardless of what type of file is being read (assuming the
        standard naming conventions)
      mass_distribution: str
        if not None, use as the mass distribution to be 
        marginalized over instead of flat in m1-m2 Currently broken
      save_likelihoods: str
        if not None, save all likelihood evaluations for every eos
        to this path
      save_marginalized_likelihoods: str
        if not None, save the marginalized eos 
        likelihood values to this path
      load_samples_kwargs : dict 
        dict of kwargs to be used in loading gw_posterior_samples, see 
        Required args section
      num_proc: int 
        the number of processors to exploit in kde evaluation.  
      num_marginalization_points: the number of m1-m2-lambda1-lambda2 samples
        to use in each marginalization 
      mc_marginalization_range: tuple of (float, float)
        The range of chirp masses to use in marginalization, 
        in M_solar (should encompass the full likelihood)
      q_marginalization_range: tuple of (float, float)
        the range of mass ratios to use in marginalization
      likelihood_bandwidth: float
        The bandwidth to be used in the kde of the astrophysical likelihood
      likelihood_prior_key : str
        The prior which was used in the generation of gw_posterior_samples
        This will be divided out when the likelihood kde was built.  
        Typically should be flat in m1 and m2 in detector frame (default)
      seed : int
        The seed to use in generation of marginalization samples
      dump_config: str
        If not None, dump a config file to  this location
        that would perform the identical lwp_pipe command when passed to 
        the script lwp-pipe
      dump_config_kwargs: Keyword arguments that may be needed for dumpimg the 
        call to lwp-pipe, for example, the location of the h5 samples 
        may not be known on disk

    Returns :
      result: dict
        dictionary with keys:
          "likelihood_evaluations" : dataframe of all likelihood evaluations
          "eos_likelihoods" : dataframe of marginalized eos likelihoods
          "prior" : string represeting mass prior used to obtain marginalized result
    """
    if dump_config is not None:
        # Short circuit the entire function, taking the arguments of the lwp_pipe call
        parameters = locals() # this is apparently not great
        parameters.pop("dump_config")
        parameters.pop("dump_config_kwargs")
        parameters.pop("retrieve_macro_data") #Can't automate this yet
        if "lwp_pipe_data_kwargs" not in dump_config_kwargs:
            raise ValueError(
                "dump_config_kwargs must contain lwp_pipe_data_kwargs,"
                "otherwise we will not know how to represent the EoS Samples in"
                             "the written config file.")
        lwp_pipe_data_kwargs = dump_config_kwargs["lwp_pipe_data_kwargs"]
        lwp_data = get_lwp_pipe_data(**parameters,
                                     lwp_pipe_data_kwargs=lwp_pipe_data_kwargs)
        if "config_kwargs" in dump_config_kwargs.keys():
            config_kwargs = dump_config_kwargs["config_kwargs"]
        else:
            config_kwargs = {}
        if "eos-indices" not in config_kwargs.keys():
            rng = np.random.default_rng(seed=seed)
            seed_tag = rng.integers(2**16-1)
            pd.DataFrame({"eos": eos_indices}).to_csv(
                f"eos_indices_{seed_tag}.csv", index=False)
            config_kwargs["eos-indices"] = f"eos_indices_{seed_tag}.csv"
        write_path = dump_config
        return lwp_data.to_config(write_path=write_path, write_config_kwargs=config_kwargs)
        
        
        
    
    if type(gw_posterior_samples) is pd.DataFrame:
        astro_columns=io.default_lwp_analysis_columns
        gw_posterior_samples = gw_posterior_samples[astro_columns]
    elif type(gw_posterior_samples) is str:
        gw_posterior_samples = io.load_samples(gw_posterior_samples,
                                               columns=astro_columns, **load_samples_kwargs)
    else:
        raise ValueError(f"Unsupported type of gw_posterior_samples: {type(gw_posterior_samples)}")

    if max_num_pe_samples is not None:
        downsample_to = min(max_num_pe_samples, gw_posterior_samples.shape[0])
        if downsample_to != gw_posterior_samples.shape[0]:
            warnings.warn("Caution is advised when downsampling, as"
                          "the precomputed bandwidth for the kde will"
                          "will likely not match the downsampled"
                        "optimal bandwidth")
            gw_posterior_samples = gw_posterior_samples.sample(
                n=downsample_to)
    
    num_eos = len(eos_indices)
    macro_files = {index : retrieve_macro_data(index) for index in eos_indices}
    io.log(verbose, "generating marginalization samples")
    marginalization_samples ={
        index :
        executables.marginalization_samples(
            macro_files[index],
            eos_index=index,
            output=None,
            mcrng=mc_marginalization_range,
            qrng=q_marginalization_range,
            save_samples=False,
            eos_column_name=_eos_column_name,
            nsamps=num_marginalization_points, seed=seed)
        for index in eos_indices
    }
    if marginalization_mass_prior is not None:
        raise ValueError(f"Case of modified marginalization prior"
                         "{marginalization_mass_prior} is not handled yet")
    # TODO : Speed up this step!
    likelihood_evaluations = {
        index :
        executables.weigh_samples(source=gw_posterior_samples,
                                  target=marginalization_samples[index],
                                  output=None,
                                  columns=["m1", "m2", "Lambda1", "Lambda2"],
                                  prior_columns = prior_columns,
                                  prior_name=likelihood_prior_key,                     
                                  bandwidth=likelihood_bandwidth,
                                  save_samples=False,
                                  num_proc=num_proc,
                                  verbose=verbose)
        for index in eos_indices
    }
    marginalized_likelihoods = {
        index :
        executables.marginalize_samples(likelihood_evaluations[index],
                                        [_eos_column_name],
                                        output_path=None,
                                        weight_column=["logweight"],
                                        weight_column_is_log=["logweight"],
                                        save_samples=False,
                                        verbose=verbose
                                        )
        for index in eos_indices
    }
    io.log(verbose, "combining all likelihoods into a single dataframe")
    likelihood_evaluations = _combine_samples(likelihood_evaluations)
    marginalized_likelihoods = _combine_samples(marginalized_likelihoods)
    if save_likelihoods is not None:
        io.log(verbose, f"saving likelihood evalutions to {save_likelihoods}")
        likelihood_evaluations.to_csv(save_likelihoods, index=False)
    if save_marginalized_likelihoods is not None:
        io.log(verbose, f"saving marginalized likelihood  evalutions to"
               "{save_marginalized_likelihoods}")
        marginalized_likelihoods.to_csv(save_marginalized_likelihoods,
                                        index=False)
    
    return {"likelihood_evaluations": likelihood_evaluations, 
            "eos_likelihoods": marginalized_likelihoods,
            "prior": _format_prior(mc_range=mc_marginalization_range,
                                   q_range=q_marginalization_range,
                                   distribution=marginalization_mass_prior)}
    
