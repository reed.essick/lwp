"""a very simple module to compute marginal likelihoods for equations of state as was done in:
[1] https://journals.aps.org/prd/abstract/10.1103/PhysRevD.99.084049 
[2] https://journals.aps.org/prd/abstract/10.1103/PhysRevD.101.063007 
[3] https://journals.aps.org/prd/abstract/10.1103/PhysRevD.101.123007 
[4] https://journals.aps.org/prd/abstract/10.1103/PhysRevD.104.063003 
[5] https://journals.aps.org/prd/abstract/10.1103/PhysRevD.105.043016
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"
