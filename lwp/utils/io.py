"""a module for basic I/O operations
"""
__author__ = "Reed Essick (reed.essick@gmail.com)"

#-------------------------------------------------

import os
import shutil

import numpy as np
import pandas as pd
import os
import json
import h5py
import warnings

from . import utils


### Import Non-standard Libraries -----------------------
import lwp
import bilby

### Want to output pandas table to replace astro_data 


#-------------------------------------------------

ALPHANUMERIC = 'a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z 0 1 2 3 4 5 6 7 8 9'.split()
DEFAULT_MAX_NUM_SAMPLES = np.infty

#-------------------------------------------------
# basic utilities for manipulating existing sapmles
#-------------------------------------------------

def column2logcolumn(name):
    return 'log(%s)'%name

def check_columns(present, required, logcolumns=[]):
    required = [column2logcolumn(column) if column in logcolumns else column for column in required]
    for column in required:
        assert column in present, 'required column=%s is missing!'%column

def load(inpath, columns=[], logcolumns=[], max_num_samples=DEFAULT_MAX_NUM_SAMPLES):
    """
    Takes either a path (and loads the data) or a 
    dataframe and return the data and column names, 
    possibly taking logs of some columns 
    """
    if isinstance(inpath, str):
        data_pd = pd.read_csv(inpath)
    else:
        data_pd = inpath
    if len(columns) != 0:
        data_pd = data_pd[columns]
    # otherwise use all of the columns 
    if max_num_samples <  data_pd.shape[0]:
        data_pd = data_pd.sample(max_num_samples)
    for column in columns:
        if column in logcolumns:
            data_to_log = data_pd.pop(column)
            data_pd[column2logcolumn(column)] = np.log(data_to_log)
    columns = list(data_pd.keys())
    data = np.array(data_pd) ### cast as an arra
    return data, columns #, dict((strind, col) for col, strind in strmap.items())

def load_weights(*args, **kwargs):
    """loads and returns weights from multiple columns via  delegation to load_logweights
    normalizes the weights while it's at it
    """
    normalize = kwargs.pop('normalize', True)
    return utils.exp_weights(load_logweights(*args, **kwargs), normalize=normalize)

def load_logweights(inpath, weight_columns, logweightcolumns=[], invweightcolumns=[], max_num_samples=DEFAULT_MAX_NUM_SAMPLES):
    """loads and returns logweights from multiple columns
    """
    data, columns = load(inpath, columns=weight_columns, max_num_samples=max_num_samples) ### load the raw data

    for i, column in enumerate(columns): ### iterate through columns, transforming as necessary
        if column in logweightcolumns:
            if column in invweightcolumns:
                data[:,i] *= -1

        else:
            if column in invweightcolumns:
                data[:,i] = 1./data[:,i]

            data[:,i] = np.log(data[:,i])

    # multiply weights across all samples, which is the same as adding the logs
    return np.sum(data, axis=1)

def write(path, data, cols, footer='', tmpdir='.', tmpdigits=6, verbose=False):
    """a standardized writing function that does an atomic write
    """
    path = os.path.abspath(path) ### make sure have the full path

    tmppath = os.path.join(tmpdir, "."+os.path.basename(path)+'-'+''.join(np.random.choice(ALPHANUMERIC, size=tmpdigits))) ### save to a temporary file
    if verbose:
        print('writing to temporary file location: '+tmppath)
    np.savetxt(tmppath, data, header=','.join(cols), comments='', delimiter=',')

    if verbose:
        print('moving to final location: '+path)
    shutil.move(tmppath, path) ### move to the permanent location

def log(to_print, message):
    if to_print:
        print(message)


default_bilby_column_mapping = {'cos_theta_jn': 'costheta_jn',
           "luminosity_distance" : "luminosity_distance_Mpc",
           "mass_1_source" : 'm1', 
           'mass_2_source' : 'm2',
           "lambda_1" : "Lambda1",
           "lambda_2" : "Lambda2",
           'spin1' : 'Spin1',
           'spin2' : 'Spin2',
           'cos_tilt_1':'costilt1',
           'cos_tilt_2':'costilt2',
           'redshift' : 'Redshift'}

def bilby_to_lwp_columns(bilby_columns, mapping=default_bilby_column_mapping):
    return [mapping[column] for column in bilby_columns]

def lwp_to_bilby_columns(lwp_columns,
                         mapping={v:k for (k,v) in
                                  default_bilby_column_mapping.items()}):
    return [mapping[column] for column in lwp_columns]

default_lwp_analysis_columns = ["m1", "m2",
                                "Lambda1", "Lambda2",
                                "luminosity_distance_Mpc"]

def load_bilby(
        result_file_name, data_type = "posterior",
        columns = lwp_to_bilby_columns(
            default_lwp_analysis_columns),
        column_mapping = default_bilby_column_mapping): #String input
    
    """
    Requirements: 
    -Bilby sample files be in the same directory as the notebook 
    -(Psuedo Requirement) User has Bilby installed
    -Assumes universal naming convention in all bilby output files
    
    Bilby result object has ONLY list values for the most part, whereas the original json file 
    loading method contained arrays of dictionaries, booleans, etc. The use of bilby to load in its' own
    result files heavily impacts the method of parsing and formating the resultant dataframe. Therefore, assuming
    each user may or may not have Bilby installed, the code has been written in such a way, that would work to
    accomodate for both methods. Further modifications could be written to take into account that bilby is being used.
    
    If "priors" is selected, there will not be a resultant dataframe returned, but instead a dictionary with the relevant
    sampling information!! 
    
    Returns: a dataframe containing the parameter estimation samples. Data collected is based on desired data, 
    respective of the 'columns' variable. Cosmological redshifts are taken into account with regards to the 
    mass values. If desired data is the posterior, then the mass values ARE source frame masses. 
    """
    
    path = f"{result_file_name}"
    
    ### Loading in Bilby files 
    results = bilby.core.result.read_in_result(path)
    
    ### Keywords for Bilby output file ** for posterior
    keywords = list(results.posterior.keys())
    
    prior_keywords = list(results.priors.keys())
    
    ### Assert that users' inputted columns match key words given for a bilby output file
    if columns is None:
        columns = lwp_to_bilby_columns(default_lwp_analysis_columns)
       
    data_column = []
    for name in columns:
        try:
            if name in keywords:
                data_column.append(name)
            elif lwp_to_bilby_columns([name])[0] in keywords:
                # If people pass the wrong column names for bilby
                # we don't want to fail, but we make a fuss
                mapped_name= lwp_to_bilby_columns([name])[0]
                warnings.warn("column named: {name} was not found in the astro"
                              "posterior however this column corresponds to a"
                              "known bilby column {mapped_name} "
                              "which was found and is being used.")
                data_column.append(mapped_name)

        except KeyError:
            if data_type == "posterior":
                raise KeyError(f"Parameter '{name}' invalid. Valid parameter names consist of: {keywords}")
            elif data_type == "priors":
                raise KeyError(f"Parameter '{name}' invalid. Valid parameter names consist of: {prior_keywords}")

    
    # To draw data from different parts of the output file 
    if data_type == "posterior":
        
        ### Assuming we DO use Bilby to load in data ---------------------------------------------------------------
        posterior = results.posterior
        
        df = pd.DataFrame()
        for name in columns:
            df[name] = posterior[name]
        
        result_df = df
        ### --------------------------------------------------------------------------------------------------------
        ### Renaming variables in the table to fit the LWP executable:
        result_df = result_df.rename(columns = column_mapping)
        
        return result_df
        
    elif data_type == "priors":
          
        ### Assuming we DO use Bilby to load in data ---------------------------------------------------------------    
        
        priors = results.priors
        
        df = pd.DataFrame()
        for name in columns:
            df[name] = priors[name]
        
        result_df = df
        
        ### --------------------------------------------------------------------------------------------------------
        
        return result_df


def load_csv(result_file_name, columns=None, column_mapping={}):
    """
    load astro samples stored as a csv (default)
    """
    if columns is None:
        columns = default_lwp_analysis_columns
    result_df = pd.read_csv(result_file_name, usecols=columns)
    return result_df.rename(column_mapping)
    
def load_public_h5_samples (result_file_name, columns=None, low_spin=True,
                            samples_subgroup="C01:IMRPhenomPv2_NRTidal:LowSpin"):
    h5_file = h5py.File(result_file_name)
    if columns is None:
        # I don't know what the column names are in an arbitrary file
        # but they're more likely to agree with bilby
        columns = lwp_to_bilby_columns(default_lwp_analysis_columns)

    samples = h5_file[samples_subgroup]

    post = np.array(samples["posterior_samples"])
    # Don't ask me why this works
    data = np.transpose(np.array([post[()][param] for param in columns]))
    return pd.DataFrame(data, columns=bilby_to_lwp_columns(columns))

def load_samples(results_filename, load_function=None, **kwargs):
    """
    Either load with load_function, or 
    correctly intuit what loading function to call based on the file extension, 
    load the samples as a dataframe.
    """
    if load_function is not None:
        return load_function(results_filename, **kwargs)
    filename, extension = os.path.splitext(results_filename)
    if extension == ".csv":
        return load_csv(results_filename, **kwargs)
    elif extension[1:] in bilby.core.result.EXTENSIONS:
        # we assume this is bilby_pe samples
        return load_bilby(results_filename, **kwargs)
    else:
        raise ValueError(f"couldn't load samples at path {results_filename}")
    
        
    
