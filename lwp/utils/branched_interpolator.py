"""
Utilities for dealing with EoSs with multiple stable branches
"""
import numpy as np
import pandas as pd
import scipy.interpolate as interpolate


def array_in(data, target_range):
    """
    return an array of shape `data.shape` which 
    stores whether each element of data is
    in the range `target_range`
    """
    (min_val, max_val) = target_range
    return np.logical_and(data>=min_val, data<=max_val)
def get_branches(eos):
    """
    Get each of the stable branches of an EoS, stored in a list.

    Each branch is a pd.DataFrame that represents the M-Lambda
    curve of the that stable branch, it has a column "branch_id"
    which represents a constant, unique, integer identifier for branch
    """
    if type(eos) != pd.DataFrame:
        if type(eos) == np.recarray:
            eos = pd.DataFrame.from_records(eos)
        else:
            # Worst case but we still cover it this is the definition
            # of what an EoS must be able to do
            eos = pd.DataFrame({"M": eos["M"], "Lambda": eos["Lambda"]})
    def segments(split_eos):
        segment_diff = split_eos.index - np.arange(split_eos.shape[0])
        return segment_diff
    # The first point is stable if the first difference is stable
    initially_stable = (np.diff(eos["M"])>=0)[0]
    stable = eos.loc[[initially_stable, *(np.diff(eos["M"])>=0)], :].copy(deep=True)
    unstable = eos.loc[[not(initially_stable), *(np.diff(eos["M"])<0)], :].copy(deep=True)
    stable["branch_id"] = segments(stable)
    branch_ids = np.unique(np.array(segments(stable)))
    branches = []
    for branch_id in branch_ids:
        branches.append(stable[stable["branch_id"] == branch_id])
        if branches[-1].shape[0] == 1:
            branch = branches[-1]
            M = np.array(branch["M"])
            Lambda = np.array(branch["Lambda"])
            # only one point on the branch
            # add a second, this branch will never
            # be used because it is too small,
            # but we keep in anyway
            M_new = [M[0], M[0]*(1+2*np.finfo(float).eps)]
            Lambda_new = [Lambda[0], Lambda[0]*(1+2*np.finfo(float).eps)]
            new_branch= pd.DataFrame({"M" : np.array(M_new),
                                      "Lambda" : np.array(Lambda_new)})
            new_branch["branch_id"] = branch_id
            branches[-1] = new_branch
    return branches

def get_lambda_interpolators(branches):
    """
    Get an interpolator for each stable branch as Lambda(M) is well defined, if an mvalue is passed which is not
    on the stable branch then a value of 0 is returned, which represents a black hole.
    """
    interpolators =  [interpolate.interp1d(np.array(branch["M"]), np.array(branch["Lambda"]), bounds_error=False, fill_value=0.0) for branch in branches]
    if len(interpolators) ==0:
        # No stable branches
        print ("This code will not work")
    return interpolators
def get_lambda_of_m_evaluations(lambda_interpolators, branches, m):
    """
    Get the values of 
    """
    results = []
    found_branches_for_ms=[]
    for i, branch in enumerate(branches):
        m_is_on_this_branch = array_in(
            m,
            (min(branch["M"]), max(branch["M"])))
        found_branches_for_ms.append(m_is_on_this_branch)        
        m_branch= m[np.where(m_is_on_this_branch)[0]]
        lambda_of_m = lambda_interpolators[i]
        results.append({"lambda":lambda_of_m(m_branch), "m": m_branch})
    #If no stable branch was found
    no_stable_branch = np.sum(np.array([*found_branches_for_ms]), axis=0) == 0
    # We set the deformability to 0 (black hole value)
    # and add an "overall unstable" branch to the results
    results.append({"m": m[no_stable_branch],"lambda" :np.zeros_like(m[no_stable_branch])})
    return results
def get_lambda_from_m_and_eos(m, eos):
    """
    Evaluate the m values on each stable branch,
    thus get interpolated Lambda(M) for each stable branch
    """
    branches = get_branches(eos)
    lambda_interpolators = get_lambda_interpolators(branches)
    lambda_of_m_evaluations = get_lambda_of_m_evaluations(lambda_interpolators, branches, m)
    return lambda_of_m_evaluations

def choose_lambda_per_m(m, eos, choice_function=None):
    """
    Get a single lambda value for each m, choosing between branches using the choice_function
    if none, sample branches randomly: do not call lambdas which are not chosen
    This will be slow, do not call it if there are not multiple stable branches.
    """
    branches = get_branches(eos)
    if len(branches) == 1:
        [stable, unstable] = get_lambda_from_m_and_eos(m, eos)
        return {"m" :np.concatenate([stable["m"], unstable["m"]]),
                "lambda":np.concatenate([stable["lambda"], unstable["lambda"]])}
    interpolators = get_lambda_interpolators(branches)
    branch_availability= np.empty((len(m),  len(branches)))
    lambdas = np.zeros_like(m)
    for i, branch in enumerate(branches):
        # each mass gets marked whether it's in the mass range of the branch
        # The + 1e-30 is a very stupid hack, basically if there is at least one branch available
        # then one of the entries is 1 and 1+1e-30 = 1
        # and in the choice call the no-brnach entries have probability 1e-30/1 = 0,
        # if no branch is available though any branch can be used with equal probability
        # 1e-30/(num_branches * 1e-30), all that will happen is this branch will report 0
        # tidal deformability
        branch_availability[:, i] = array_in(m,  (min(branch["M"]), max(branch["M"]))) + 1e-30
    for j, m_val in enumerate(m):
        branch_to_use = np.random.choice(
            np.arange(len(branches)), p = branch_availability[j, :]/np.sum(branch_availability[j, :]))
        lambdas[j] = interpolators[branch_to_use](m_val)
    return {"lambda" :lambdas, "m": m}
    
        

if __name__ == "__main__":
    print("Example")
    # A silly example as a test case
    # TODO factor this into a real test
    rhoc = np.linspace(1.0, 3.0, 100)
    test_M = (rhoc-2)**3 - (rhoc-2) + 1
    test_Lambda = 1000*(rhoc+1)**-6
    test_eos = pd.DataFrame({"M" : test_M, "Lambda":test_Lambda})
    print("branches:", get_branches(test_eos))
    print("evaluating on [.7, 1.0, 1.3]")
    print(get_lambda_from_m_and_eos(np.array([.7, 1.0, 1.3]), test_eos))
    print(choose_lambda_per_m(np.array([.7, 1.0, 1.3]), test_eos))
